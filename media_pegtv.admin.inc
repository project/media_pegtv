<?php

/**
 *  @file
 *  Administrative page callbacks for Media: PEG.TV.
 */

/**
 *  Callback for /media/add/media_pegtv and
 *  /admin/content/media/add/media_pegtv.
 */
function media_pegtv_add($form, &$form_state = array(), $redirect = NULL) {
  global $user;

  $form = array();
  $form['pegtv'] = array(
    '#type' => 'vertical_tabs',
  );
  $form['pegtv']['all'] = array(
    '#type' => 'fieldset',
    '#title' => t('All PEG.TV videos'),
  );

  // Get all pegtv files for this user
  $results = db_query("SELECT fid FROM {file_managed} WHERE uid = :uid AND uri LIKE :uri", array(
    ':uid' => $user->uid,
    ':uri' => 'pegtv%%'
  ))->fetchAll();

  foreach ($results as $result) {
    $file = file_load($result->fid);
    $output = theme('image', array(
      //http://origin.peg.tv/thumbnails/19526.jpg
      'path' => 'http://origin.peg.tv/thumbnails/' . pathinfo($file->uri, PATHINFO_FILENAME) . '.jpg',
      'title' => 'title',
      'alt' => 'alt',
      'attributes' => array('width' => 150),
      'getsize' => FALSE,
    ));
    $form['pegtv']['all'][$file->fid] = array(
      '#markup' => $output,
    );
  }

/*  $form['pegtv']['all']['test'] = array(
    '#type' => 'checkbox',
    '#title' => 'test',
  );*/
  $form['pegtv']['add_from_url'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add from URL'),
  );
  $form['pegtv']['add_from_url']['url'] = array(
    '#type' => 'textfield',
    '#title' => 'URL',
    '#description' => 'Input the URL of the desired PEG.TV video page.',
  );
  $form['redirect'] = array(
    '#type' => 'value',
    '#value' => $redirect,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  return $form;
}

/**
 *  Validation for media_pegtv_add().
 */
function media_pegtv_add_validate($form, &$form_state) {
  //http://origin.peg.tv/pegtv_player?id=T01531&video=19526
  if (!preg_match('@peg\.tv/pegtv_player?id=([^"\& ]+)@i', $form_state['values']['url'], $matches)) {
    form_set_error('url', t('Please submit a valid PEG.TV video URL.'));
  }
}

/**
 *  Submission for media_pegtv_add().
 *
 *  This will create a file object for the PEG.TV video.
 */
function media_pegtv_add_submit($form, &$form_state) {
  $defaults = array (
    'display' => TRUE,
  );

  $uri = media_pegtv_media_parse($form_state['values']['url']);

  // Check to see if this a duplicate of an existing file
  $files = file_load_multiple(NULL, array('uri' => $uri));
  if ($files) {
    // This is ugly.
    $file = array_shift($files);
  }
  else {
    // @TODO: Will this work for PEG.TV??
    // copy($url, $destination);
    $file = file_uri_to_object($uri);
    file_save($file);
  }

  // field_attach_insert('media', $file);
  if ($file) {
    $form_state['redirect'] = 'media/' . $file->fid . '/edit';
    field_attach_submit('media', $file, $form, $form_state);
    field_attach_insert('media', $file);
  }
  else {
    drupal_set_message(t('An error occurred and no file was saved.'), 'error');
  }

  $form_state['redirect'] = !empty($form_state['values']['redirect']) ? $form_state['values']['redirect'] : 'media/' . $file->fid . '/edit';
}
