<?php

/**
 * @file media_pegtv/includes/media_pegtv.styles.inc
 * Styles definitions for Media: PegTV.
 */

/**
 * Implementation of Styles module hook_styles_register().
 */
function media_pegtv_styles_register() {
  return array(
    'MediaPegTVStyles' => array(
      'field_types' => 'file',
      'name' => t('MediaPegTV'),
      'description' => t('Media PegTV styles.'),
      'path' => drupal_get_path('module', 'media_pegtv') .'/includes',
      'file' => 'media_pegtv.styles.inc',
    ),
  );
}

/**
 *  Implements hook_styles_containers(). (Deprecated in version 2)
 */
function media_pegtv_styles_containers() {
  return array(
    'file' => array(
      'containers' => array(
        'media_pegtv' => array(
          'label' => t('PegTV Styles'),
          'data' => array(
            'streams' => array(
              'pegtv',
            ),
            'mimetypes' => array(
              'video/pegtv',
            ),
          ),
          'weight' => 0,
          'filter callback' => 'media_pegtv_formatter_filter',
          'themes' => array(
            'field_formatter_styles' => 'media_pegtv_field_formatter_styles',
            'styles' => 'media_pegtv_styles',
            'preview' => 'media_pegtv_preview_style',
          ),
          'description' => t('PegTV Styles will display embedded PegTV videos and thumbnails to your choosing, such as by resizing, setting colors, and autoplay. You can !manage.', array('!manage' => l(t('manage your PegTV styles here'), 'admin/config/media/media-pegtv-styles'))),
        ),
      ),
    ),
  );
}

function media_pegtv_formatter_filter($variables) {
  if (isset($variables['object'])) {
    $object = isset($variables['object']->file) ? $variables['object']->file : $variables['object'];
    return (file_uri_scheme($object->uri) == 'pegtv') && ($object->filemime == 'video/pegtv');
 
  }
}

/**
 * Implementation of the File Styles module's hook_file_styles_filter().
 */
function media_pegtv_file_styles_filter($object) {
  $file = isset($object->file) ? $object->file : $object;
  if ((file_uri_scheme($file->uri) == 'pegtv') && ($file->filemime == 'video/pegtv')) {
    return 'media_pegtv';
  }
}

/**
 *  Implements hook_styles_styles().
 */
function media_pegtv_styles_styles() {
  $styles = array(
    'file' => array(
      'containers' => array(
        'media_pegtv' => array(
          'styles' => array(
            'pegtv_thumbnail' => array(
              'name' => 'pegtv_thumbnail',
              'effects' => array(
                array('label' => t('Thumbnail'), 'name' => 'thumbnail', 'data' => array('thumbnail' => 1)),
                array('label' => t('Resize'), 'name' => 'resize', 'data' => array('width' => 100, 'height' => 75)),
              ),
            ),
            'pegtv_preview' => array(
              'name' => 'pegtv_preview',
              'effects' => array(
                array('label' => t('Autoplay'), 'name' => 'autoplay', 'data' => array('autoplay' => 0)),
                array('label' => t('Resize'), 'name' => 'resize', 'data' => array('width' => 220, 'height' => 165)),
              ),
            ),
            'pegtv_full' => array(
              'name' => 'pegtv_full',
              'effects' => array(
                array('label' => t('Autoplay'), 'name' => 'autoplay', 'data' => array('autoplay' => 0)),
                array('label' => t('Resize'), 'name' => 'resize', 'data' => array('width' => 640, 'height' => 480)),
                array('label' => t('Full screen'), 'name' => 'fullscreen', 'data' => array('fullscreen' => 1)),
              ),
            ),
          ),
        ),
      ),
    ),
  );

  // Allow any image style to be applied to the thumbnail.
  foreach (image_styles() as $style_name => $image_style) {
    $styles['file']['containers']['media_pegtv']['styles']['pegtv_thumbnail_' . $style_name] = array(
      'name' => 'pegtv_thumbnail_' . $style_name,
      'image_style' => $style_name,
      'effects' => array(
        array('label' => t('Thumbnail'), 'name' => 'thumbnail', 'data' => array('thumbnail' => 1)),
      ),
    );
  }

  return $styles;
}

/**
 *  Implements hook_styles_presets().
 */
function media_pegtv_styles_presets() {
  $presets = array(
    'file' => array(
      'square_thumbnail' => array(
        'media_pegtv' => array(
          'pegtv_thumbnail_square_thumbnail',
        ),
      ),
      'thumbnail' => array(
        'media_pegtv' => array(
          'pegtv_thumbnail',
        ),
      ),
      'small' => array(
        'media_pegtv' => array(
          'pegtv_preview',
        ),
      ),
      'large' => array(
        'media_pegtv' => array(
          'pegtv_full',
        ),
      ),
      'original' => array(
        'media_pegtv' => array(
          'pegtv_full',
        ),
      ),
    ),
  );
  return $presets;
}

/**
 * Implementation of Styles module hook_styles_default_containers().
 */
function media_pegtv_styles_default_containers() {
  // We append PegTV to the file containers.
  return array(
    'file' => array(
      'containers' => array(
        'media_pegtv' => array(
          'class' => 'MediaPegTVStyles',
          'name' => 'media_pegtv',
          'label' => t('PegTV'),
          'preview' => 'media_pegtv_preview_style',
        ),
      ),
    ),
  );
}


/**
 * Implementation of Styles module hook_styles_default_presets().
 */
function media_pegtv_styles_default_presets() {
  $presets = array(
    'file' => array(
      'containers' => array(
        'media_pegtv' => array(
          'default preset' => 'unlinked_thumbnail',
          'styles' => array(
            'original' => array(
              'default preset' => 'video',
            ),
            'thumbnail' => array(
              'default preset' => 'linked_thumbnail',
            ),
            'square_thumbnail' => array(
              'default preset' => 'linked_square_thumbnail',
            ),
            'medium' => array(
              'default preset' => 'linked_medium',
            ),
            'large' => array(
              'default preset' => 'large_video',
            ),
          ),
          'presets' => array(
            'video' => array(
              array(
                'name' => 'video',
                'settings' => array(),
              ),
            ),
            'large_video' => array(
              array(
                'name' => 'resize',
                'settings' => array(
                  'width' => 640,
                  'height' => 390,
                ),
              ),
              array(
                'name' => 'video',
                'settings' => array(),
              ),
            ),
          ),
        ),
      ),
    ),
  );
  // Allow any image style to be applied to the thumbnail.
  foreach (image_styles() as $style_name => $image_style) {
    $presets['file']['containers']['media_pegtv']['presets']['linked_' . $style_name] = array(
      array(
        'name' => 'linkToMedia',
        'settings' => array(),
      ),
      array(
        'name' => 'imageStyle',
        'settings' => array(
          'image_style' => $style_name,
        ),
      ),
      array(
        'name' => 'thumbnail',
        'settings' => array(),
      ),
    );
    $presets['file']['containers']['media_pegtv']['presets']['unlinked_' . $style_name] = $presets['file']['containers']['media_pegtv']['presets']['linked_' . $style_name];
    array_shift($presets['file']['containers']['media_pegtv']['presets']['unlinked_' . $style_name]);
  }
  return $presets;
}
