<?php

/**
 *  @file
 *  Create a PegTV Stream Wrapper class for the Media/Resource module.
 */

/**
 *  Create an instance like this:
 *  $pegtv = new ResourcePegTVStreamWrapper('pegtv://?v=[video-code]');
 */
class MediaPegTVStreamWrapper extends MediaReadOnlyStreamWrapper {
  protected $base_url = 'http://origin.peg.tv/pegtv_player/';

  function getTarget($f) {
    return FALSE;
  }

  static function getMimeType($uri, $mapping = NULL) {
    return 'video/pegtv';
  }

  function getOriginalThumbnailPath() {
    $parts = $this->get_parameters();
    //$data = media_pegtv_data($parts['video_id'], $parts['client_id']);
    //return $data['thumbnail']['url'];
    //dsm($parts);
    watchdog('media_pegtv', 'http://origin.peg.tv/thumbnails/'. check_plain($parts['video']) .'.jpg');
    return 'http://origin.peg.tv/thumbnails/'. check_plain($parts['video']) .'.jpg';
    
  }

  function getLocalThumbnailPath() {
    
    $parts = $this->get_parameters();
    //dsm($parts);
    $local_path = 'public://media-pegtv/' . check_plain($parts['video']) . '.jpg';
    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      @copy($this->getOriginalThumbnailPath(), $local_path);
    }
    return $local_path;
  }
}

