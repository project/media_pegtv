
/**
 * @file media_pegtv/js/media_pegtv.js
 */

(function ($) {

Drupal.media_pegtv = {};
Drupal.behaviors.media_pegtv = {
  attach: function (context, settings) {
    // Check the browser to see if it supports html5 video.
    var video = document.createElement('video');
    var html5 = video.canPlayType ? true : false;

    // If it has video, does it support the correct codecs?
    if (html5) {
      html5 = false;
      if (video.canPlayType( 'video/webm; codecs="vp8, vorbis"' ) || video.canPlayType('video/mp4; codecs="avc1.42E01E, mp4a.40.2"')) {
        html5 = true;
      }
    }

    // Put a prompt in the video wrappers to let users know they need flash
    if (!FlashDetect.installed && !html5){
      $('.media-pegtv-preview-wrapper').each(Drupal.media_pegtv.needFlash);
    }
  }
};

Drupal.media_pegtv.needFlash = function () {
  var id = $(this).attr('id');
  var wrapper = $('.media-pegtv-preview-wrapper');
  var hw = Drupal.settings.media_pegtv[id].height / Drupal.settings.media_pegtv[id].width;
  wrapper.html('<div class="js-fallback">' + Drupal.t('You need Flash to watch this video. <a href="@flash">Get Flash</a>', {'@flash':'http://get.adobe.com/flashplayer'}) + '</div>');
  wrapper.height(wrapper.width() * hw);
};

Drupal.media_pegtv.insertEmbed = function (embed_id) {
  var videoWrapper = $('#' + embed_id + '.media-pegtv-preview-wrapper');
  var settings = Drupal.settings.media_pegtv[embed_id];

  // Calculate the ratio of the dimensions of the embed.
  settings.hw = settings.height / settings.width;

  // Replace the object embed with PegTV's iframe. This isn't done by the
  // theme function because PegTV doesn't have a no-JS or no-Flash fallback.
  var video = $('<iframe class="pegtv-player" type="text/html" frameborder="0"></iframe>');
  //http://origin.peg.tv/pegtv_player?id=T01631&video=24316&noplaylistskin=1&width=400&height=300
  //var src = 'http://www.peg.tv/embed/' + settings.video_id;
  var src = 'http://origin.peg.tv/pegtv_player?id=T01631&video=' + settings.video_id + '&noplaylistskin=1&width=400&height=300';
  // Allow other modules to modify the video settings.
  settings.options.wmode = 'opaque';
  $(window).trigger('media_pegtv_load', settings);

  // Merge PegTV options (such as autoplay) into the source URL.
  var query = $.param(settings.options);
  if (query) {
    src += '?' + query;
  }

  // Set up the iframe with its contents and add it to the page.
  video
    .attr('id', settings.id)
    .attr('width', settings.width)
    .attr('height', settings.height)
    .attr('src', src);
  videoWrapper.html(video);

  // Bind a resize event to handle fluid layouts.
  $(window).bind('resize', Drupal.media_pegtv.resizeEmbeds);

  // For some reason Chrome does not properly size the container around the
  // embed and it will just render the embed at full size unless we set this
  // timeout.
  if (!$('.lightbox-stack').length) {
    setTimeout(Drupal.media_pegtv.resizeEmbeds, 1);
  }
};

Drupal.media_pegtv.resizeEmbeds = function () {
  $('.media-pegtv-preview-wrapper').each(Drupal.media_pegtv.resizeEmbed);
};

Drupal.media_pegtv.resizeEmbed = function () {
  var context = $(this).parent();
  var video = $(this).children(':first-child');
  var hw = Drupal.settings.media_pegtv[$(this).attr('id')].hw;
  // Change the height of the wrapper that was given a fixed height by the
  // PegTV theming function.
  $(this)
    .height(context.width() * hw)
    .width(context.width());

  // Change the attributes on the embed to match the new size.
  video
    .height(context.width() * hw)
    .width(context.width());
};

})(jQuery);