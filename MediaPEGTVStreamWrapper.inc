<?php

/**
 *  @file
 *  Create a PEG.TV Stream Wrapper class for the Media/Resource module.
 */

/**
 *  Create an instance like this:
 *  $pegtv = new ResourcePEGTVStreamWrapper('pegtv://?id=[video-code]');
 */
class MediaPEGTVStreamWrapper extends MediaReadOnlyStreamWrapper {
  //http://origin.peg.tv/pegtv_player?id=T01531&video=19526&noplaylistskin=1&width=400&height=300
  //http://origin.peg.tv/pegtv_player?id=T01531&video=19526
  //http://origin.peg.tv/pegtv_player?player_config=T01531&noplaylistskin=1&width=400&height=300&video=19526
  //http://origin.peg.tv/thumbnails/19526.jpg
  protected $base_url = 'http://origin.peg.tv/pegtv_player/';

  function getTarget($f) {
    return FALSE;
  }

  static function getMimeType($uri, $mapping = NULL) {
    return 'video/pegtv';
  }

  function getOriginalThumbnailPath() {
    $parts = $this->get_parameters();
    //dsm($parts);
    return 'http://origin.peg.tv/thumbnails/'. check_plain($parts['video']) .'.jpg';
  }

  function getLocalThumbnailPath() {
    $parts = $this->get_parameters();
    $local_path = 'public://media-pegtv/' . check_plain($parts['video']) . '.jpg';
    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      @copy($this->getOriginalThumbnailPath(), $local_path);
    }
    return $local_path;
  }
}
